#lang racket

;; 1.9
(define remove
  (lambda (s los)
    (if (null? los)
        '()
        (if (eqv? (car los) s)
            (remove s (cdr los))
            (cons (car los) (remove s (cdr los)))))))

;; 1.13
(define subst-in-s-exp
  (lambda (new old sexp)
    (if (symbol? sexp) 
        (if (eqv? sexp old) new sexp)
        (subst new old sexp))))

(define subst
  (lambda (new old slist)
  (map (lambda (exp) (subst-in-s-exp new old exp)) slist)))

;; 1.17

(define (down lst)
  (map (lambda (el) (list el)) lst))

;;(down '(a b (c)))

;; 1.19
(define (list-set lst n x)
  (if (eqv? n 0)
      (cons x (cdr lst))
      (cons
       (car lst)
       (list-set (cdr lst) (- n 1) x))))

;;(list-set '(a b c d) 2 '(1 2))

;; 1.21

(define (product sos1 sos2)
  (foldl
   (lambda (s1 acc)
     (append
      (map (lambda (s2) (list s1 s2)) sos2)
      acc))
   '()
   sos1
   ))

;;(product '(a b c) '(x y))

;; 1.22

(define filter-in
  (lambda (pred lst)
    (if (null? lst)
        '()
        (if (pred (car lst))
            (cons (car lst) (filter-in pred (cdr lst)))
            (filter-in pred (cdr lst))))))

;;(filter-in number? '(a v 1 c (1 3) b 7))

;; 1.23

(define (list-index pred lst)
  (define (list-search-n lst n)
    (if (null? lst)
        #f
        (if (pred (car lst))
            n
            (list-search-n (cdr lst) (+ n 1)))))
  (list-search-n lst 0))

;;(list-index number? '(a b 1))
;;(list-index number? '(a b c))

(define (flatten slist)
  (define (flatten-sexp sexp)
    (if (symbol? sexp)
        (list sexp)
        (flatten sexp)))
  (if (null? slist)
      '()
      (append
       (flatten-sexp (car slist))
       (flatten (cdr slist)))))

;;(flatten '((a b) (() (e ())) c d))


;; 1.31

(define (leaf n) n)
(define (interior-node sym right left) (list sym right left))
(define (leaf? node) (number? node))
(define (lson node) (list-ref node 1))
(define (rson node) (list-ref node 2))
(define (contents-of node)
  (if (number? node)
      node
      (car node)))

;; 1.34
(define (path n bst)
  (cond
    ((eqv? bst '()) '("not-found")) ;; format differs from one in 1.31
    ((= n (contents-of bst)) '())
    ((leaf? bst) '("not-found"))
    ((< n (contents-of bst))
     (cons "left" (path n (lson bst))))
    ((> n (contents-of bst))
     (cons "right" (path n (rson bst))))
    ))

;;(path 17 '(14 (7 () (12 () ()))
;;              (26 (20 (17 () ())
;;                      ())
;;                  (31 () ()))))