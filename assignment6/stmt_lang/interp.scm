(module interp (lib "eopl.ss" "eopl")
  
  ;; interpreter for the IMPLICIT-REFS language

  (require "drscheme-init.scm")

  (require "lang.scm")
  (require "data-structures.scm")
  (require "environments.scm")
  (require "store.scm")

  (require racket/list)
  
  (provide value-of-program value-of-exp instrument-let instrument-newref)

;;;;;;;;;;;;;;;; switches for instrument-let ;;;;;;;;;;;;;;;;

  (define instrument-let (make-parameter #f))

  ;; say (instrument-let #t) to turn instrumentation on.
  ;;     (instrument-let #f) to turn it off again.

;;;;;;;;;;;;;;;; the interpreter ;;;;;;;;;;;;;;;;

  ;; value-of-program : Program -> ExpVal
  (define value-of-program 
    (lambda (pgm)
      (initialize-store!)
      (cases program pgm
        (a-program (stmt)
          (value-of-stmt stmt (init-env))))))

  (define (value-of-vardecl decl env)
    (cases var-decl decl
           (var-no-init (var) (list var 42))
           (var-with-init (var exp) (list var (value-of-exp exp env)))))
  (define (name-of-vardecl decl)
     (cases var-decl decl
            (var-no-init (var) var)
            (var-with-init (var exp) var)))

  (define (extend-env* vars env)
    (if (null? vars)
        env
        (extend-env*
         (cdr vars)
         (extend-env
          (name-of-vardecl (car vars))
          (newref 42)
          env))))
  (define (populate-env vars env-rec)
     (if (null? vars)
         '()
         (let ((var (value-of-vardecl (car vars) env-rec)))
           (begin
             (setref!
              (apply-env env-rec (car var))
              (cadr var))
             (populate-env (cdr vars) env-rec)))))

  (define (value-of-stmt stmt env)
    (cases
     statement stmt

     (assign-stmt
      (var exp)
      (begin
        (setref!
         (apply-env env var)
         (value-of-exp exp env))))

     (print-stmt
      (exp)
      (cases expval (value-of-exp exp env)
             (num-val (v) (eopl:printf "~v\n" v))
             (bool-val (v) (eopl:printf "~v\n" v))
             (proc-val (v) (eopl:printf "<proc>"))
             (ref-val (v) (eopl:printf "ref<~v>" v))))

      (compound-stmt
       (stmts)
       (map
        (lambda (s) (value-of-stmt s env))
        stmts))
      (if-stmt
       (exp stmt1 stmt2)
       (let ((val (value-of-exp exp env)))
         (if (expval->bool val)
             (value-of-stmt stmt1 env)
             (value-of-stmt stmt2 env))))

      (while-stmt
       (exp body)
        (let ((val (value-of-exp exp env)))
          (if (expval->bool val)
              (begin
                (value-of-stmt body env)
                (value-of-stmt stmt env)) ; eval while again
              '())))

      (var-stmt
       (vars body)
       (let ((env-rec (extend-env* vars env)))
         (begin
           (populate-env vars env-rec)
           (value-of-stmt body env-rec))))
      ))
      

  ;; value-of-exp : Exp * Env -> ExpVal
  ;; Page: 118, 119
  (define value-of-exp
    (lambda (exp env)
      (cases expression exp

        ;\commentbox{ (value-of-exp (const-exp \n{}) \r) = \n{}}
        (const-exp (num) (num-val num))

        ;\commentbox{ (value-of-exp (var-exp \x{}) \r) 
        ;              = (deref (apply-env \r \x{}))}
        (var-exp (var) (deref (apply-env env var)))

        ;\commentbox{\diffspec}
        (diff-exp (exp1 exp2)
          (let ((val1 (value-of-exp exp1 env))
                (val2 (value-of-exp exp2 env)))
            (let ((num1 (expval->num val1))
                  (num2 (expval->num val2)))
              (num-val
                (- num1 num2)))))

        ;\commentbox{\zerotestspec}
        (zero?-exp (exp1)
          (let ((val1 (value-of-exp exp1 env)))
            (let ((num1 (expval->num val1)))
              (if (zero? num1)
                (bool-val #t)
                (bool-val #f)))))
              
        ;\commentbox{\ma{\theifspec}}
        (if-exp (exp1 exp2 exp3)
          (let ((val1 (value-of-exp exp1 env)))
            (if (expval->bool val1)
              (value-of-exp exp2 env)
              (value-of-exp exp3 env))))

        ;\commentbox{\ma{\theletspecsplit}}
        (let-exp (var exp1 body)       
          (let ((v1 (value-of-exp exp1 env)))
            (value-of-exp body
              (extend-env var (newref v1) env))))
        
        (proc-exp (var body)
          (proc-val (procedure var body env)))

        (call-exp (rator rand)
          (let ((proc (expval->proc (value-of-exp rator env)))
                (arg (value-of-exp rand env)))
            (apply-procedure proc arg)))

        (letrec-exp (p-names b-vars p-bodies letrec-body)
          (value-of-exp letrec-body
            (extend-env-rec* p-names b-vars p-bodies env)))

        (begin-exp (exp1 exps)
          (letrec 
            ((value-of-begins
               (lambda (e1 es)
                 (let ((v1 (value-of-exp e1 env)))
                   (if (null? es)
                     v1
                     (value-of-begins (car es) (cdr es)))))))
            (value-of-begins exp1 exps)))

        (assign-exp (var exp1)
          (begin
            (setref!
              (apply-env env var)
              (value-of-exp exp1 env))
            (num-val 27)))

        )))


  ;; apply-procedure : Proc * ExpVal -> ExpVal
  ;; Page: 119

  ;; uninstrumented version
  ;;  (define apply-procedure
  ;;    (lambda (proc1 val)
  ;;      (cases proc proc1
  ;;        (procedure (var body saved-env)
  ;;          (value-of-exp body
  ;;            (extend-env var (newref val) saved-env))))))
  
  ;; instrumented version
  (define apply-procedure
    (lambda (proc1 arg)
      (cases proc proc1
        (procedure (var body saved-env)
          (let ((r (newref arg)))
            (let ((new-env (extend-env var r saved-env)))
              (when (instrument-let)
                (begin
                  (eopl:printf
                    "entering body of proc ~s with env =~%"
                    var)
                  (pretty-print (env->list new-env)) 
                  (eopl:printf "store =~%")
                  (pretty-print (store->readable (get-store-as-list)))
                  (eopl:printf "~%")))
              (value-of-exp body new-env)))))))  

  ;; store->readable : Listof(List(Ref,Expval)) 
  ;;                    -> Listof(List(Ref,Something-Readable))
  (define store->readable
    (lambda (l)
      (map
        (lambda (p)
          (list
            (car p)
            (expval->printable (cadr p))))
        l)))

  )
  


  
