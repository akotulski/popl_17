(module tests mzscheme
  
  (provide test-list)

  ;;;;;;;;;;;;;;;; tests ;;;;;;;;;;;;;;;;
  
  (define test-list
    '(
  
      ;; simple arithmetic
      (positive-const "11" 11)
      (negative-const "-33" -33)
      (simple-arith-1 "-(44,33)" 11)
  
      ;; nested arithmetic
      (nested-arith-left "-(-(44,33),22)" -11)
      (nested-arith-right "-(55, -(22,11))" 44)
  
      ;; simple variables
      (test-var-1 "x" 10)
      (test-var-2 "-(x,1)" 9)
      (test-var-3 "-(1,x)" -9)
      
      ;; simple unbound variables
      (test-unbound-var-1 "foo" error)
      (test-unbound-var-2 "-(x,foo)" error)
  
      ;; simple conditionals
      (if-true "if zero?(0) then 3 else 4" 3)
      (if-false "if zero?(1) then 3 else 4" 4)
      
      ;; test dynamic typechecking
      (no-bool-to-diff-1 "-(zero?(0),1)" error)
      (no-bool-to-diff-2 "-(1,zero?(0))" error)
      (no-int-to-if "if 1 then 2 else 3" error)

      ;; make sure that the test and both arms get evaluated
      ;; properly. 
      (if-eval-test-true "if zero?(-(11,11)) then 3 else 4" 3)
      (if-eval-test-false "if zero?(-(11, 12)) then 3 else 4" 4)
      
      ;; and make sure the other arm doesn't get evaluated.
      (if-eval-test-true-2 "if zero?(-(11, 11)) then 3 else foo" 3)
      (if-eval-test-false-2 "if zero?(-(11,12)) then foo else 4" 4)

      ;; simple let
      (simple-let-1 "let x = 3 in x" 3)

      ;; make sure the body and rhs get evaluated
      (eval-let-body "let x = 3 in -(x,1)" 2)
      (eval-let-rhs "let x = -(4,1) in -(x,1)" 2)

      ;; check nested let and shadowing
      (simple-nested-let "let x = 3 in let y = 4 in -(x,y)" -1)
      (check-shadowing-in-body "let x = 3 in let x = 4 in x" 4)
      (check-shadowing-in-rhs "let x = 3 in let x = -(x,1) in x" 2)

      ;; 3.9
      (empty-list-val "empty-list" ())
      (cons-list-val "cons(2,empty-list)" (2))
      (list-of-list-val "cons(cons(2,empty-list), cons (2, empty-list))" ((2) 2))
      (list-of-bool "cons(zero?(0), cons(zero?(1),empty-list))" (#t #f))
      (empty-list-null? "null?(empty-list)" #t)
      (list-cons-null? "null?(cons(2, empty-list))" #f)
      (simple-list-cons-car "car(cons(2, empty-list))" 2)
      (eval-list-cons-car "car(cons(if zero?(2) then -(10,1) else -(20,2), empty-list))" 18)
      (cdr-of-cons-null? "null?(cdr(cons(2, empty-list)))" #t)
      (cdr-of-cons-cons-val? "car(cdr(cons(2, cons(3,empty-list))))" 3)
      (let-list-assign "let l = cons(2, empty-list) in car(l)" 2)

      ;; 3.10
      (list-empty "list()" ())
      (list-two-elements "list(1, 2)" (1 2))
      (list-eval-elements "let x = 4 in list(x, -(x, 2), -(0, x))" (4 2 -4))
      (list-of-list "list(list (1, 2, 3), list(2), list())" ((1 2 3) (2) ()))
      (list-null1 "null?(list())" #t)
      (list-null2 "null?(list(1))" #f)
      (list-car "car(list(1,2,3))" 1)
      (list-cdr "cdr(list(1,2,3))" (2 3))

      ;; 3.17
      (let*-zero "let* in -(4,1)" 3)
      (let*-one "let* x = -(4,1) in -(x,1)" 2)
      (let*-dependent "let* x = 3 y = -(x, 2) in -(x,y)" 2)
      (let*-independent "let* x = 3 y = 1 in -(x,y)" 2)
      (let*-nested1 "let x = 3 in let* x = 1 y = x in y" 1)
      (let*-nested2 "let x = 3 in let* y = x x = 1 in y" 3)

      ))
  )