(module environments (lib "eopl.ss" "eopl") 
  
  ;; builds environment interface, using data structures defined in
  ;; data-structures.scm. 

  (require "data-structures.scm")

  (provide init-env apply-env)

;;;;;;;;;;;;;;;; initial environment ;;;;;;;;;;;;;;;;

  ;; init-env : () -> Env
  ;; usage: (init-env) = [i=1, v=5, x=10]
  ;; (init-env) builds an environment in which i is bound to the
  ;; expressed value 1, v is bound to the expressed value 5, and x is
  ;; bound to the expressed value 10.
  ;; Page: 69
  (define init-env 
    (lambda ()
      (extend-env 
       'i (eager (num-val 1))
       (extend-env
        'v (eager (num-val 5))
        (extend-env
         'x (eager (num-val 10))
         (empty-env))))))

;;;;;;;;;;;;;;;; environment observers ;;;;;;;;;;;;;;;;

  (define (apply-env env search-sym)
    (cases environment env
      (empty-env () (eopl:error 'apply-env "No binding for ~s" search-sym))
      (extend-env
       (var val saved-env) 
       (if
        (eqv? search-sym var)
        val
        (apply-env saved-env search-sym)))))

  )