
(module utils (lib "eopl.ss" "eopl")

	(provide equal??)

  ;; simple-minded magic for tests
  (define-syntax equal??
    (syntax-rules ()
      ((_ x y)
       (let ((x^ x) (y^ y))
         (if (not (equal? x y))
           (eopl:error 'equal??
             "~s is not equal to ~s" 'x 'y)
             #t)))))
)
