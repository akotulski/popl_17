(module translator (lib "eopl.ss" "eopl")
  
  (require "lang.scm")

  (provide translation-of-program)
  ;;;;;;;;;;;;;;;; lexical address calculator ;;;;;;;;;;;;;;;;

  ;; translation-of-program : Program -> Nameless-program
  ;; Page: 96
  (define translation-of-program
    (lambda (pgm)
      (cases program pgm
        (a-program (exp1)
          (a-program                    
            (translation-of exp1 (init-senv)))))))

  ;; translation-of : Exp * Senv -> Nameless-exp
  ;; Page 97
  (define translation-of
    (lambda (exp senv)
      (cases expression exp
        (const-exp (num) (const-exp num))
        (diff-exp (exp1 exp2)
          (diff-exp
            (translation-of exp1 senv)
            (translation-of exp2 senv)))
        (zero?-exp (exp1)
          (zero?-exp
            (translation-of exp1 senv)))
        (if-exp (exp1 exp2 exp3)
          (if-exp
            (translation-of exp1 senv)
            (translation-of exp2 senv)
            (translation-of exp3 senv)))
        (var-exp (var)
          (nameless-var-exp
            (apply-senv senv var)))
        (let-exp (var exp1 body)
          (nameless-let-exp
            (translation-of exp1 senv)            
            (translation-of body
              (extend-senv var senv))))
        (proc-exp (var body)
          (let* ((trim-senv-result (trim-senv 0 var body senv))
                (source-idx (map car trim-senv-result))
                (proc-senv (map cadr trim-senv-result)))
            (nameless-proc-exp
              (translation-of body
                (extend-senv var proc-senv))
              source-idx)))
        (call-exp (rator rand)
          (call-exp
            (translation-of rator senv)
            (translation-of rand senv)))
        (else (report-invalid-source-expression 'value-of exp))
        )))


  (define (is-captured? exp search-var)
    (cases expression exp
      (const-exp (num) #f)
      (var-exp (var) (eqv? var search-var))
      (diff-exp
       (exp1 exp2)
       (or
        (is-captured? exp1 search-var)
        (is-captured? exp2 search-var)))
      (zero?-exp (exp1) (is-captured? exp1 search-var))
      (if-exp
       (exp1 exp2 exp3)
       (or
        (is-captured? exp1 search-var)
        (is-captured? exp2 search-var)
        (is-captured? exp3 search-var)))
      (let-exp
       (var exp1 body)
       (if (eqv? var search-var)
           (is-captured? exp1 search-var)
           (or (is-captured? exp1 search-var) (is-captured? body search-var))))
      (proc-exp
       (var body)
       (if (eqv? var search-var)
           #f
           (is-captured? body search-var)))
      (call-exp
       (rator rand)
       (or
        (is-captured? rator search-var)
        (is-captured? rand search-var)))
        (else (report-invalid-source-expression 'is-captured? exp))
    ))

  (define (trim-senv idx bvar body senv)
    (if (null? senv)
      '()
      (if (and
            (not (eqv? (car senv) bvar))
            (is-captured? body (car senv)))
        (cons (list idx (car senv)) (trim-senv (+ 1 idx) bvar body (cdr senv)))
        (trim-senv (+ 1 idx) bvar body (cdr senv)))))

  (define report-invalid-source-expression
    (lambda (fname exp)
      (eopl:error fname
        "Illegal expression in source code: ~s" exp)))
  
   ;;;;;;;;;;;;;;;; static environments ;;;;;;;;;;;;;;;;
  
  ;;; Senv = Listof(Sym)
  ;;; Lexaddr = N

  ;; empty-senv : () -> Senv
  ;; Page: 95
  (define empty-senv
    (lambda ()
      '()))

  ;; extend-senv : Var * Senv -> Senv
  ;; Page: 95
  (define extend-senv
    (lambda (var senv)
      (cons var senv)))
  
  ;; apply-senv : Senv * Var -> Lexaddr
  ;; Page: 95
  (define apply-senv
    (lambda (senv var)
      (cond
        ((null? senv) (report-unbound-var var))
        ((eqv? var (car senv))
         0)
        (else
          (+ 1 (apply-senv (cdr senv) var))))))

  (define report-unbound-var
    (lambda (var)
      (eopl:error 'translation-of "unbound variable in code: ~s" var)))

  ;; init-senv : () -> Senv
  ;; Page: 96
  (define init-senv
    (lambda ()
      (extend-senv 'i
        (extend-senv 'v
          (extend-senv 'x
            (empty-senv))))))
  
  )
