(module environments (lib "eopl.ss" "eopl") 
  
  ;; builds environment interface, using data structures defined in
  ;; data-structures.scm. 

  (require "data-structures.scm")

  (provide init-env empty-env extend-env extend-env-rec apply-env)

;;;;;;;;;;;;;;;; initial environment ;;;;;;;;;;;;;;;;
  
  ;; init-env : () -> Env
  ;; usage: (init-env) = [i=1, v=5, x=10]
  ;; (init-env) builds an environment in which i is bound to the
  ;; expressed value 1, v is bound to the expressed value 5, and x is
  ;; bound to the expressed value 10.
  ;; Page: 69

  (define init-env 
    (lambda ()
      (extend-env 
       'i (num-val 1)
       (extend-env
        'v (num-val 5)
        (extend-env
         'x (num-val 10)
         (empty-env))))))

;;;;;;;;;;;;;;;; environment constructors and observers ;;;;;;;;;;;;;;;;
 
  ;; for given expression:
  ;; letrec p1(x) = ... p2(x) = ... p3(x) = ... in
  ;; 
  ;; extend-env-rec builds following env structure
  ;; (note env3 in every line!!!)
  ;; env3 = extend-env p3 [proc(var1, body1, env3)] env2
  ;; env2 = extend-env p2 [proc(var2, body2, env3)] env1
  ;; env1 = extend-env p1 [proc(var3, body3, env3)] saved-env
  (define (extend-env-rec p-names b-vars bodys saved-env)
    (if (null? p-names)
      saved-env
      (let* ((vec (make-vector 1))
             (new-env (extend-env (car p-names) vec saved-env))
             (result-env (extend-env-rec (cdr p-names) (cdr b-vars) (cdr bodys) new-env)))
            (vector-set! vec 0 (proc-val (procedure (car b-vars) (car bodys) result-env)))
            result-env)))

  ; version from the book for 3.35, not suitable for 3.36
  ;(define (extend-env-rec p-name b-var body saved-env)
  ;  (let ((vec (make-vector 1)))
  ;        (let ((new-env (extend-env p-name vec saved-env)))
  ;          (vector-set! vec 0 (proc-val (procedure b-var body new-env)))
  ;          new-env)))

  ; Solution of 3.35
  (define (extract-expval val-or-vector)
    (if (vector? val-or-vector)
      (vector-ref val-or-vector 0)
      val-or-vector))

  (define apply-env
    (lambda (env search-sym)
      (cases environment env
        (empty-env ()
          (eopl:error 'apply-env "No binding for ~s" search-sym))
        (extend-env
          (var val saved-env)
          (if (eqv? search-sym var)
            (extract-expval val)
	          (apply-env saved-env search-sym))))))
    
  )
