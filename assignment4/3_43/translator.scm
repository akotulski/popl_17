(module translator (lib "eopl.ss" "eopl")
  
  (require "lang.scm")

  (provide translation-of-program)
  ;;;;;;;;;;;;;;;; lexical address calculator ;;;;;;;;;;;;;;;;

  ;; translation-of-program : Program -> Nameless-program
  ;; Page: 96
  (define translation-of-program
    (lambda (pgm)
      (cases program pgm
        (a-program (exp1)
          (a-program                    
            (translation-of exp1 (init-senv)))))))

  ;; translation-of : Exp * Senv -> Nameless-exp
  ;; Page 97
  (define translation-of
    (lambda (exp senv)
      (cases expression exp
        (const-exp (num) (const-exp num))
        (diff-exp (exp1 exp2)
          (diff-exp
            (translation-of exp1 senv)
            (translation-of exp2 senv)))
        (zero?-exp (exp1)
          (zero?-exp
            (translation-of exp1 senv)))
        (if-exp (exp1 exp2 exp3)
          (if-exp
            (translation-of exp1 senv)
            (translation-of exp2 senv)
            (translation-of exp3 senv)))
        (var-exp (var)
            (apply-senv senv var 0))
        (let-exp (var exp1 body)
          (translation-of body
            (extend-senv-let-expr
              var
              (translation-of exp1 senv)
              senv)))
                        
        (proc-exp (var body)
          (nameless-proc-exp
            (translation-of body
              (extend-senv-bound-var var senv))))
        (call-exp (rator rand)
          (call-exp
            (translation-of rator senv)
            (translation-of rand senv)))
        (else (report-invalid-source-expression exp))
        )))

  (define report-invalid-source-expression
    (lambda (exp)
      (eopl:error 'value-of 
        "Illegal expression in source code: ~s" exp)))
  
   ;;;;;;;;;;;;;;;; static environments ;;;;;;;;;;;;;;;;
  
  ;;; Senv = Listof(Sym)
  ;;; Lexaddr = N

  ;; empty-senv : () -> Senv
  ;; Page: 95
  (define-datatype senv senv?
    (empty-senv)
    (extend-senv-bound-var
      (var symbol?)
      (saved-senv senv?))
    (extend-senv-let-expr
      (var symbol?)
      (expr expression?)
      (saved-senv senv?)))
  
  ;; apply-senv : Senv * Var * Int -> Expr
  ;; Page: 95
  (define (apply-senv s-env var depth)
    (cases senv s-env
      (empty-senv () (report-unbound-var var))
      (extend-senv-bound-var
        (saved-var saved-senv)
        (if (eqv? var saved-var)
          (nameless-var-exp depth)
          (apply-senv saved-senv var (+ 1 depth))))
      (extend-senv-let-expr
        (saved-var saved-expr saved-senv)
        (if (eqv? var saved-var)
          saved-expr
          (apply-senv saved-senv var depth)))))

  (define report-unbound-var
    (lambda (var)
      (eopl:error 'translation-of "unbound variable in code: ~s" var)))

  ;; init-senv : () -> Senv
  ;; Page: 96
  (define init-senv
    (lambda ()
      (extend-senv-bound-var 'i
        (extend-senv-bound-var 'v
          (extend-senv-bound-var 'x
            (empty-senv))))))
  
  )
