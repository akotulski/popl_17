(module translator (lib "eopl.ss" "eopl")
  
  (require "lang.scm")

  (provide translation-of-program)
  ;;;;;;;;;;;;;;;; lexical address calculator ;;;;;;;;;;;;;;;;

  ;; translation-of-program : Program -> Nameless-program
  ;; Page: 96
  (define translation-of-program
    (lambda (pgm)
      (cases program pgm
        (a-program (exp1)
          (a-program                    
            (translation-of exp1 (init-senv)))))))

  ;; translation-of : Exp * Senv -> Nameless-exp
  ;; Page 97
  (define translation-of
    (lambda (exp senv)
      (cases expression exp
        (const-exp (num) (const-exp num))
        (diff-exp (exp1 exp2)
          (diff-exp
            (translation-of exp1 senv)
            (translation-of exp2 senv)))
        (zero?-exp (exp1)
          (zero?-exp
            (translation-of exp1 senv)))
        (if-exp (exp1 exp2 exp3)
          (if-exp
            (translation-of exp1 senv)
            (translation-of exp2 senv)
            (translation-of exp3 senv)))
        (var-exp (var) (apply-senv senv var))
        (let-exp (var exp1 body)
          (nameless-let-exp
            (translation-of exp1 senv)            
            (translation-of body
              (extend-senv var senv))))
        (proc-exp (var body)
          (nameless-proc-exp
            (translation-of body
              (extend-senv var senv))))
        (call-exp (rator rand)
          (call-exp
            (translation-of rator senv)
            (translation-of rand senv)))
        (letrec-exp
          (pname bvar p-body letrec-body)
          (nameless-letrec-exp
            (translation-of
              p-body
              (extend-senv bvar (extend-letrec-senv pname senv)))
            (translation-of
              letrec-body
              (extend-letrec-senv pname senv))))

        (else (report-invalid-source-expression exp))
        )))

  (define report-invalid-source-expression
    (lambda (exp)
      (eopl:error 'value-of 
        "Illegal expression in source code: ~s" exp)))
  
   ;;;;;;;;;;;;;;;; static environments ;;;;;;;;;;;;;;;;
  
  ;;; Senv = Listof(Sym)
  ;;; Lexaddr = N

  (define-datatype senv senv?
    (empty-senv)
    (extend-senv
      (var symbol?)
      (saved-senv senv?))
    (extend-letrec-senv
      (pname symbol?)
      (saved-senv senv?)))

  (define (inc-senv-res result)
    (cases expression result
      (nameless-var-exp (val) (nameless-var-exp (+ 1 val)))
      (nameless-letrec-var-exp (val) (nameless-letrec-var-exp (+ 1 val)))
      (else
         (eopl:error 'inc-senv-res "Illegal expression in senv: ~s" result))))

  ;; apply-senv : Senv * Var -> Lexaddr
  ;; Page: 95
  (define (apply-senv s var)
    (cases senv s
      (empty-senv () report-unbound-var var)
      (extend-senv
        (saved-var saved-senv)
        (if (eqv? var saved-var)
          (nameless-var-exp 0)
          (inc-senv-res (apply-senv saved-senv var))))
      (extend-letrec-senv
        (saved-pname saved-senv)
        (if (eqv? var saved-pname)
          (nameless-letrec-var-exp 0)
          (inc-senv-res (apply-senv saved-senv var))))))

  (define report-unbound-var
    (lambda (var)
      (eopl:error 'translation-of "unbound variable in code: ~s" var)))

  ;; init-senv : () -> Senv
  ;; Page: 96
  (define init-senv
    (lambda ()
      (extend-senv 'i
        (extend-senv 'v
          (extend-senv 'x
            (empty-senv))))))
  
  )
