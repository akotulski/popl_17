(module environments (lib "eopl.ss" "eopl") 
  
  ;; builds environment interface, using data structures defined in
  ;; data-structures.scm. 

  (require "data-structures.scm")

  (provide init-env empty-env extend-env extend-env-rec apply-env)

;;;;;;;;;;;;;;;; initial environment ;;;;;;;;;;;;;;;;
  
  ;; init-env : () -> Env
  ;; usage: (init-env) = [i=1, v=5, x=10]
  ;; (init-env) builds an environment in which i is bound to the
  ;; expressed value 1, v is bound to the expressed value 5, and x is
  ;; bound to the expressed value 10.
  ;; Page: 69

  (define init-env 
    (lambda ()
      (extend-env 
       'i (num-val 1)
       (extend-env
        'v (num-val 5)
        (extend-env
         'x (num-val 10)
         (empty-env))))))

;;;;;;;;;;;;;;;; environment constructors and observers ;;;;;;;;;;;;;;;;

  ; copy paste from eopl3/chapter2/sec2.2-proc-rep.scm
  (define (empty-env)
    (lambda (search-var)
      (eopl:error 'apply-env "No binding for ~s" search-var)))

  (define (extend-env saved-var saved-val saved-env)
    (lambda (search-var)
      (if (eqv? search-var saved-var)
        saved-val
        (apply-env saved-env search-var))))

  ; 3.34
  (define (extend-env-rec pname bvar body saved-env)
    (lambda (search-var)
      (if (eqv? search-var pname)
        (proc-val (procedure bvar body (extend-env-rec pname bvar body saved-env)))
        (apply-env saved-env search-var))))

  (define apply-env
    (lambda (env search-var) 
      (env search-var)))
    
  )
