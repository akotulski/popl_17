#!/bin/bash

opam install --yes \
menhir \
jbuilder \
ounit \
core \
ppx_compare \
ocamlformat

(cd src && jbuilder build run.exe test.exe)
