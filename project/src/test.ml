open OUnit2

let eval ast = Eval.value_of_prog ([], ast)

let parse code = Util.parse_with_error (Lexing.from_string code)

let peval code = parse code |> Eval.value_of_prog

let i v = Ast.IntVal v

let vi v = Ast.Value (i v)

let v value = Ast.Value value

let string_of_value (v: Eval.value) = Format.asprintf "%a" Eval.pp_value v

let suite =
  "suite"
  >::: [ ( "eval_value"
         >:: fun ctx -> assert_equal (Eval.IntVal 10) (eval (vi 10)) )
       ; ( "eval_minus"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 7) (eval (Ast.Minus (vi 10, vi 3))) )
       ; ( "eval_times"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 30) (eval (Ast.Times (vi 10, vi 3))) )
       ; ( "call_fun1"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 10)
           (eval (Ast.Apply (Ast.Lambda ("x", Ast.Var "x" |> v) |> v, vi 10)))
         )
       ; ( "call_fun2"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 9)
           (eval
              (Ast.Apply
                 ( Ast.Lambda ("x", Ast.Minus (Ast.Var "x" |> v, vi 2)) |> v
                 , vi 11 ))) )
       ; ( "fun_plus"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 22)
           (eval
              (Ast.Apply
                 ( Ast.Apply
                     ( Ast.Lambda
                         ( "x"
                         , Ast.Lambda
                             ( "y"
                             , Ast.Minus
                                 ( Ast.Var "x" |> v
                                 , Ast.Minus (vi 0, Ast.Var "y" |> v) ) )
                           |> v )
                       |> v
                     , vi 10 )
                 , vi 12 ))) )
       ; ( "parse_int"
         >:: fun ctx -> assert_equal (Eval.IntVal 10) (peval "effects {} 10")
         )
       ; ( "parse_bool"
         >:: fun ctx ->
         assert_equal (Eval.BoolVal false) (peval "effects {} false") )
       ; ( "parse_minus"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 8) (peval "effects {} (- 10 2)") )
       ; ( "parse_times"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 20) (peval "effects {} (* 10 2)") )
       ; ( "parse_equal"
         >:: fun ctx ->
         assert_equal (Eval.BoolVal true) (peval "effects {} (== 2 2)") )
       ; ( "parse_function_call"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 8)
           (peval "effects {} (lambda x. (- x 2) 10)") )
       ; ( "basic_if"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 8)
           (peval "effects {} if true then 8 else 10") )
       ; ( "if_in_function"
         >:: fun ctx ->
         assert_equal (Eval.IntVal (-20))
           (peval
              "effects {} (lambda f. (* (f 20) (f 0)) lambda x. if (== x 0) then -1 else x)")
         )
       ; ( "parse_effect_decls"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 10)
           (peval
              "effects {effect1 [op1;op2], effect2[op]} handle(effect1, 10, [op1 p r. p; op2 p r. 10], return x. x)")
         )
       ; ( "parse_unit"
         >:: fun ctx -> assert_equal Eval.Unit (peval "effects {} ()") )
       ; ( "basic_let"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 2) (peval "effects {} let x = 3 in (- x 1)")
         )
       ; ( "let_static_binding"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 10)
           (peval
              "effects {} let a = 10 in let f = lambda x. a in let a = 3 in (f 2)")
         )
       ; ( "basic_letrec"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 50)
           (peval
              "effects {} let rec times10 = x . if (== x 0) then 0 else (- (times10 (- x 1)) -10) in (times10 5)")
         )
       ; ( "basic_handle"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 18)
           (peval
              "effects {e [op]}handle (e,(e:op 10),[op p r. (r (- p 1))],return x. (* x 2))")
         )
       ; ( "lift_of_exp"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 18) (peval "effects {e []} lift(e, 18)") )
       ; ( "handle_unknown_effect"
         >:: fun ctx ->
         assert_raises (Eval.EvalException "unknown effect name") (fun () ->
             peval "effects {} handle(e, 18, [], return x.x)" ) )
       ; ( "lift_unknown_effect"
         >:: fun ctx ->
         assert_raises (Eval.EvalException "unknown effect name") (fun () ->
             peval "effects {} lift(e, 18)" ) )
       ; ( "unhandled_op"
         >:: fun ctx ->
         assert_raises (Eval.EvalException "couldn't find handler") (fun () ->
             peval "effects {e [op]} (e:op ())" ) )
       ; ( "nested_function"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 18)
           (peval "effects {} (lambda x. (lambda z. (- x (- 0 z)) 10) 8)") )
       ; ( "nested_handlers"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 9)
           (peval
              "effects {e []} handle(e, handle(e, 10, [], return x. x), [], return x. (-x 1))")
         )
       ; ( "nested_handlers2"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 1)
           (peval
              "effects {e [op]} handle(e, handle(e, (e:op ()), [op x r. (r 1)], return x. x), [op x r. (r 2)], return x. x)")
         )
       ; ( "basic_lift"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 2)
           (peval
              "effects {e [op]} handle(e, handle(e, lift(e, (e:op ())), [op x r. (r 1)], return x. x), [op x r. (r 2)], return x. x)")
         )
       ; ( "lift_different_effect"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 1)
           (peval
              "effects {e [op], e2 []} handle(e, handle(e, lift(e2, (e:op ())), [op x r. (r 1)], return x. x), [op x r. (r 2)], return x. x)")
         )
       ; ( "lift_twice"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 2)
           (peval
              {|effects {e [op]}
                handle(e,
                  handle(e,
                    handle(e,
                      lift(e, lift(e, (e:op ()))),
                      [op x r. (r 0)],
                      return x. x),
                    [op x r. (r 1)],
                    return x. x),
                  [op x r. (r 2)],
                  return x. x)
               |})
         )
       ; ( "lift_twice2"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 2)
           (peval
              {|effects {e [op]}
                handle(e,
                  handle(e,
                    lift(e, handle(e, // that lift will be reached when looking for the handler
                      lift(e, (e:op ())),
                      [op x r. (r 0)],
                      return x. x)),
                    [op x r. (r 1)],
                    return x. x),
                  [op x r. (r 2)],
                  return x. x)
               |})
         )
       ; ( "lift_once"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 1)
           (peval
              {|effects {e [op]}
                handle(e,
                  lift(e, handle(e, // that lift won't be reached, it will stop on the handler below the lift
                    handle(e,
                      lift(e, (e:op ())),
                      [op x r. (r 0)],
                      return x. x),
                    [op x r. (r 1)],
                    return x. x)),
                  [op x r. (r 2)],
                  return x. x)
               |})
         )
       ; ( "op_no_call"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 30)
           (peval
              "effects {e [op]} handle(e, (handle(e, e:op, [op p r. (* 2 p)], return x. x) 10), [op p r. (* 3 p)], return x.x)")
         )
       ; ( "call_same_fn_twice"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 110)
           (peval
              {|effects {e [op]} (lambda f .
                   (* handle(e, (f 10), [op p r. (- p 2)], return x.x)
                      handle(e, (f 11), [op p r. (- p 8)], return x.x))
               lambda x . x)|})
         )
       ; ( "different_handlers_same_fn_with_resume"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 20)
           (peval
              {|effects {e [op]} (lambda f .
                   (* handle(e, (f 10), [op p r. (r p)], return x.x)
                      handle(e, (f 10), [op p r. (r (-p 8))], return x.x))
               lambda x . (e:op x))|})
         )
       ; ( "different_handlers_same_fn_no_resume"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 20)
           (peval
              {|effects {e [op]} (lambda f .
                   (* handle(e, (f 10), [op p r. p], return x.x)
                      handle(e, (f 10), [op p r. (-p 8)], return x.x))
               lambda x . (e:op x))|})
         )
       ; ( "different_effect_handlers"
         >:: fun ctx ->
         assert_equal (Eval.IntVal 9)
           (peval
              {|effects {e1 [op], e2 [op]}
                handle(e1,
                       handle(e2, (e1:op 10), [op p r. (* p 2)], return x.x),
                       [op p r. (- p 1)], // should run this handler
                       return x.x)|})
         )
       ; ( "resume_non_empty_stack"
         >:: fun ctx ->
         assert_equal (Eval.IntVal (-40))
           (peval
              "effects {e [op]} handle(e, (* (* 10 (e:op 2)) 2), [op p r. (* (r p) -1)], return x.x)")
         )
       ; ( "resume_twice_non_empty_stack"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 20)
           (* = 10 * 2 * 2 - 10 * 1 * 2 *)
           (peval
              "effects {e [op]} handle(e, (* (* 10 (e:op 2)) 2), [op p r. (- (r p) (r 1))], return x.x)")
         )
       ; ( "no_resume_non_empty_stack"
         >:: fun ctx ->
         assert_equal (Eval.IntVal (-2))
           (peval
              "effects {e [op]} handle(e, (* (* 10 (e:op 2)) 2), [op p r. (* p -1)], return x.x)")
         )
       ; ( "resume_with_correct_env"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 6)
           (peval
              "effects {e [op]} (lambda x. handle(e, (lambda x . (* (e:op 2) x) 3), [op p r. (r p)], return x.x) 20)")
         )
       ; ( "resume_with_correct_env2"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 7)
           (peval
              {|effects {e [op]}
               (lambda x.
                  handle(e,
                         (lambda x . ((e:op 3) x) 10),
                         [op p r. (r lambda x. (- x p))],
                         return x.x)
                20)|})
         )
       ; ( "resume_with_correct_env3"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 7)
           (peval
              {|effects {e [op]} let x = 9
                                 in handle(e,
                                    let x = 3 in (- (e:op 10) x),
                                    [op y r. (r y)],
                                    return z.z)
              |})
         )
       ; ( "resume_with_correct_env4"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 6)
           (peval
              {|effects {e [op]} let x = 9
                                 in handle(e,
                                    let x = 3 in (- (e:op 10) x),
                                    [op y r. (r x)], // resume with x (= 9) instead of y (= 10)
                                    return z.z)
              |})
         )
       ; ( "resume_with_same_handler"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 12)
           (peval
              {|effects {e [op]}
               handle(e,
                 handle(e, (* (e:op 3) (e:op 4)), [op x r. (r x)], return x.x),
                [op x r. (r 1)],
                return x.x)|})
         )
       ; ( "resume_with_same_handler2"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal (6 * 16))
           (peval
              {|effects {e [op]}
               handle(e,
                 handle(e, (* (e:op 3) (e:op (e:op 4))), [op x r. (r (* 2 x))], return x.x),
                [op x r. (r 1)],
                return x.x)|})
         )
       ; ( "state_effect_get"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 44)
           (peval
              {|effects {state [get; set]}
                 (handle(state,
                   (state:get ()),
                   [get x r. lambda s. ((r s) s);
                    set x r. lambda s. ((r ()) x)],
                   return x. lambda u.x) 44)
               |})
         )
       ; ( "state_effect_set"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value Eval.Unit
           (peval
              {|effects {state [get; set]}
                 (handle(state,
                   (state:set 10),
                   [get x r. lambda s. ((r s) s);
                    set x r. lambda s. ((r ()) x)],
                   return x. lambda u.x) 44)
               |})
         )
       ; ( "state_effect_set_get"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 10)
           (peval
              {|effects {state [get; set]}
                 (handle(state,
                   (lambda z. (state:get ()) (state:set 10)),
                   [get x r. lambda s. ((r s) s);
                    set x r. lambda s. ((r ()) x)],
                   return x. lambda u.x) 44)
               |})
         )
       ; ( "state_effect_get_set_get"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 440)
           (peval
              {|effects {state [get; set]}
                 (handle(state,
                   (* (state:get ())
                      (lambda z. (state:get ()) (state:set 10))),
                   [get x r. lambda s. ((r s) s);
                    set x r. lambda s. ((r ()) x)],
                   return x. lambda u.x) 44)
               |})
         )
       ; ( "state_effect_outside_f"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 41)
           (peval
              {|effects {state [get; set]}
                 (lambda f. // f x = get() - (set(x); get())
                   (handle(state,
                     (f 3),
                     [get x r. lambda s. ((r s) s);
                      set x r. lambda s. ((r ()) x)],
                     return x. lambda u.x) 44)
                 lambda x . (- (state:get ())
                               (lambda z. (state:get ()) (state:set x))))
               |})
         )
       ; ( "fake_state_effect_outside_f"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 0)
           (peval
              {|effects {state [get; set]}
                 (lambda f. // f x = get() - (set(x); get())
                   handle(state, // this handler always returns 44
                     (f 3),
                     [get x r. (r 44);
                      set x r. (r ())],
                     return x. x)
                 lambda x . (- (state:get ())
                               (lambda z. (state:get ()) (state:set x))))
               |})
         )
       ; ( "monadic_increment"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 13)
           (peval
              {|effects {state [get; set]}
                 let init_state = lambda s. lambda f. lambda x.
                   (handle(state,
                     (f x),
                     [get x r. lambda s. ((r s) s);
                      set x r. lambda s. ((r ()) x)],
                     return x. lambda u.x) s)
                 in let increment_fn = // increment state by one
                    lambda blah.
                      let next = (- (state:get ()) -1) in let blah = (state:set next) in next
                 in let exec2 = lambda f1. lambda f2. lambda x. (f2 (f1 x)) // function application
                 in let increment_twice_fn = ((exec2 increment_fn) increment_fn)
                 in let increment_three_fn = ((exec2 increment_twice_fn) increment_fn)
                 in let increment_twice = lambda st. ((st increment_three_fn) ())
                 in (increment_twice (init_state 10))
               |})
         )
       ; ( "increment2"
         >:: fun ctx ->
         assert_equal ~printer:string_of_value (Eval.IntVal 12)
           (peval
              {|effects {state [get; set]}
                 let init_state = lambda s. lambda f. lambda x.
                   (handle(state,
                     (f x),
                     [get x r. lambda s. ((r s) s);
                      set x r. lambda s. ((r ()) x)],
                     return x. lambda u.u) s) // THIS DIFFERS FROM PREVIOUS HANDLERS!!!!
                 in let increment_fn = // increment state by one
                    lambda blah.
                      let next = (- (state:get ()) -1) in (state:set next)
                 // apply_to_state returns new state after applying function to it
                 in let apply_to_state = lambda st. lambda f. (init_state ((st f) ()))
                 in let newstate = ((apply_to_state
                       ((apply_to_state (init_state 10)) increment_fn))
                       increment_fn)
                 in ((newstate lambda x.x) ()) // this will return value in the state
               |})
         ) ]


let _ = run_test_tt_main suite
