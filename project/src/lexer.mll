
{
  open Lexing
  open Parser

  exception SyntaxError of string

  let next_line lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      { pos with pos_bol = lexbuf.lex_curr_pos;
                pos_lnum = pos.pos_lnum + 1;
                pos_cnum = 1;
      }
}

let comment  = "//" [^'\n']*
let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let id = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*
let int = '-'? ['0'-'9'] ['0'-'9']*

rule read =
  parse
  | white { read lexbuf }
  | comment { read lexbuf }
  | newline  { next_line lexbuf; read lexbuf }
  | int { INT (int_of_string (Lexing.lexeme lexbuf)) }
  | "(" { LEFT_PAREN }
  | ")" { RIGHT_PAREN }
  | "[" { LEFT_BRACKET }
  | "]" { RIGHT_BRACKET }
  | "{" { LEFT_BRACE }
  | "}" { RIGHT_BRACE }
  | ":" { COLON }
  | ";" { SEMICOLON }
  | "." { DOT }
  | "," { COMMA }
  | "return" { RETURN }
  | "lift" { LIFT }
  | "handle" { HANDLE }
  | "lambda" { LAMBDA }
  | "effects" { EFFECTS }
  | "-" { MINUS }
  | "*" { STAR }
  | "=" { EQUAL }
  | "if" { IF }
  | "then" { THEN }
  | "else" { ELSE }
  | "let" { LET }
  | "let rec" { LETREC }
  | "in" { IN }
  | "true" { TRUE }
  | "false" { FALSE }
  | id { IDENTIFIER (Lexing.lexeme lexbuf) }
  | _ { raise (SyntaxError ("Unexpected char: '" ^ (Lexing.lexeme lexbuf) ^ "'")) }
  | eof { EOF }
