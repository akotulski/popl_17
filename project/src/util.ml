

let parse_with_error lexbuf =
  try Parser.prog Lexer.read lexbuf with
  | Lexer.SyntaxError msg -> Printf.fprintf stderr "%s\n" msg ; exit (-1)
  | Parser.Error ->
      Printf.fprintf stderr "parser error@\n" ;
      exit (-1)
