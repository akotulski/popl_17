
%token LEFT_PAREN
%token RIGHT_PAREN
%token LEFT_BRACKET
%token RIGHT_BRACKET
%token RIGHT_BRACE
%token LEFT_BRACE
%token COLON
%token SEMICOLON
%token DOT
%token COMMA
%token RETURN
%token LIFT
%token HANDLE
%token EFFECTS
%token LAMBDA
%token MINUS
%token STAR
%token EQUAL
%token IF
%token THEN
%token ELSE
%token TRUE
%token FALSE
%token LET
%token LETREC
%token IN
(*
%token POLY_ABS
%token POLY_INST
 *)
%token <int> INT
%token <string> IDENTIFIER
%token EOF

%start <Ast.prog> prog
%%

prog:
  | EFFECTS; LEFT_BRACE; eff_decls = separated_list(COMMA, effect_decl); RIGHT_BRACE; e = exp EOF { (eff_decls, e) }

effect_decl:
  | n = IDENTIFIER; LEFT_BRACKET; ops = separated_list(SEMICOLON, IDENTIFIER); RIGHT_BRACKET { (n, ops) }
exp:
  | v = value {Ast.Value v}
  | LEFT_PAREN; rator = exp; rand = exp; RIGHT_PAREN { Ast.Apply (rator, rand) }
  (*  | POLY_INST; LEFT_PAREN; e = exp; RIGHT_PAREN {Ast.PolyInst e} *)
  | LIFT; LEFT_PAREN; n = IDENTIFIER; COMMA; e = exp; RIGHT_PAREN { Ast.Lift (n, e) }
  | HANDLE; LEFT_PAREN; n = IDENTIFIER; COMMA; e = exp; COMMA; h = eff_handler; COMMA; RETURN; ret_id = IDENTIFIER; DOT; ret_exp = exp; RIGHT_PAREN {Ast.Handle (n, e, h, ret_id, ret_exp) };
  | LEFT_PAREN; MINUS; e1 = exp; e2 = exp; RIGHT_PAREN { Ast.Minus (e1, e2) }
  | LEFT_PAREN; STAR; e1 = exp; e2 = exp; RIGHT_PAREN { Ast.Times (e1, e2) }
  | LEFT_PAREN; EQUAL; EQUAL; e1 = exp; e2 = exp; RIGHT_PAREN { Ast.CmpEqual (e1, e2) }
  | IF; cond = exp; THEN e1 = exp; ELSE; e2 = exp { Ast.IfElse (cond, e1, e2) }
  | LET; v = IDENTIFIER; EQUAL; e1 = exp; IN; e2 = exp { Ast.Let (v, e1, e2) }
  | LETREC; pname = IDENTIFIER; EQUAL; arg = IDENTIFIER; DOT; pbody = exp; IN; e = exp { Ast.Let (pname, Ast.Value (Ast.LambdaRec (pname, arg, pbody)), e) }

value:
  | name = IDENTIFIER { Ast.Var name }
  | LAMBDA; var = IDENTIFIER; DOT; body = exp { Ast.Lambda (var, body) }
  (*| POLY_ABS; LEFT_PAREN; e = exp; RIGHT_PAREN {Ast.PolyAbstraction e }*)
  | eff = IDENTIFIER; COLON; op = IDENTIFIER {Ast.Operation (op, eff)}
  | LEFT_PAREN; RIGHT_PAREN {Ast.Unit}
  | TRUE {Ast.True}
  | FALSE {Ast.False}
  | v = INT { Ast.IntVal v } ;

eff_handler:
  | LEFT_BRACKET; h = separated_list(SEMICOLON, effect); RIGHT_BRACKET {h};

effect:
  | op = IDENTIFIER; operation_arg = IDENTIFIER; resume_cont = IDENTIFIER; DOT; body = exp
             {Ast.{op; operation_arg; resume_cont; body}};


