open Core

type ident = string [@@deriving compare]

module IdentOrd = struct
  type t = ident

  let compare = compare_ident
end

module IdentMap = Caml.Map.Make (IdentOrd)

type eff_name = string [@@deriving compare]

type op_name = string [@@deriving compare]

type effect_decl = string * string list [@@deriving compare]

type value =
  | Var of ident
  | Lambda of ident * exp
  | LambdaRec of ident * ident * exp
  (*| PolyAbstraction of exp*)
  | Operation of op_name * eff_name
  | IntVal of int
  | Unit
  | True
  | False
  [@@deriving compare]

and effect =
  {op: op_name; operation_arg: ident; resume_cont: ident; body: exp}
  [@@deriving compare]

and eff_handler = effect list [@@deriving compare]

(*operation arg * resume continuation * ?? *)
and exp =
  | Value of value
  | Apply of exp * exp
  (*| PolyInst of exp (* ??? - polymorphic instantiation *)*)
  | Lift of eff_name * exp
  (* ??? should it include effect name 'l' as well? *)
  | Handle of eff_name * exp * eff_handler * ident * exp
  (* last two are for return handler*)
  | Minus of exp * exp
  | Times of exp * exp
  | CmpEqual of exp * exp
  | IfElse of exp * exp * exp
  | Let of ident * exp * exp
  [@@deriving compare]

let eff_name_equal = [%compare.equal : eff_name]

let op_name_equal = [%compare.equal : op_name]

type prog = effect_decl list * exp

let fprintf = Format.fprintf

let rec pp_exp fmt = function
  | Value v -> pp_val fmt v
  | Apply (rator, rand) -> fprintf fmt "call(%a, %a)" pp_exp rator pp_exp rand
  | Lift (eff, e) -> fprintf fmt "lift(%s, %a)" eff pp_exp e
  | Handle (eff, body, handler, ret_id, ret_exp) ->
      fprintf fmt "handle(%s, %a, %a, return %s. %a)" eff pp_exp body
        pp_handler handler ret_id pp_exp ret_exp
  | Minus (e1, e2) -> fprintf fmt "(- %a %a)" pp_exp e1 pp_exp e2
  | Times (e1, e2) -> fprintf fmt "(* %a %a)" pp_exp e1 pp_exp e2
  | CmpEqual (e1, e2) -> fprintf fmt "(== %a %a)" pp_exp e1 pp_exp e2
  | IfElse (c, e1, e2) ->
      fprintf fmt "if %a then %a else %a" pp_exp c pp_exp e1 pp_exp e2
  | Let (var, e1, e2) ->
      fprintf fmt "let %s = %a in %a" var pp_exp e1 pp_exp e2


and pp_val fmt = function
  | Var id -> fprintf fmt "%s" id
  | Lambda (id, e) -> fprintf fmt "lambda %s . %a" id pp_exp e
  | LambdaRec (pname, id, e) ->
      fprintf fmt "rec %s = %s . %a" pname id pp_exp e
  | Operation (op, eff) -> fprintf fmt "%s:%s" eff op
  | IntVal v -> fprintf fmt "int: %d" v
  | Unit -> fprintf fmt "()"
  | True -> fprintf fmt "true"
  | False -> fprintf fmt "false"


and pp_handler fmt handler =
  List.iter ~f:(fun effect -> fprintf fmt "%a;" pp_effect effect) handler


and pp_effect fmt effect =
  fprintf fmt "%s %s, %s . %a" effect.op effect.operation_arg
    effect.resume_cont pp_exp effect.body


let pp fmt (_, body) = pp_exp fmt body
