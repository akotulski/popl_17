open Printf


let main () =
  let inx = stdin in
  let lexbuf = Lexing.from_channel inx in
  let ast = Util.parse_with_error lexbuf in
  Format.fprintf Format.std_formatter "%a@\n" Ast.pp ast ;
  Format.fprintf Format.std_formatter "%a@\n" Eval.pp_value
    (Eval.value_of_prog ast)


let _ = main ()
