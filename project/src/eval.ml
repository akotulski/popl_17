open Core

module BuiltinsDecl = struct
  type t = Minus | Times | Equal [@@deriving compare]
end

type value =
  | IntVal of int
  | BoolVal of bool
  | Closure of Ast.ident * Ast.exp * env_t
  | RecClosure of Ast.ident * Ast.ident * Ast.exp * env_t
  | ResumeClosure of context_t
  | Operation of Ast.op_name * Ast.eff_name
  | Unit
  [@@deriving compare]

and context_cell =
  | Rator of env_t * Ast.exp
  | Apply of value
  (*| PolyInst of context *)
  | Lift of Ast.eff_name
  | Handle of handle_context_data
  | TwoArgF1 of BuiltinsDecl.t * env_t * Ast.exp
  | TwoArgF2 of BuiltinsDecl.t * value
  | Conditional of env_t * Ast.exp * Ast.exp
  | LetCtx of env_t * Ast.ident * Ast.exp
  [@@deriving compare]

and handle_context_data =
  { env: env_t
  ; eff: Ast.eff_name
  ; handler: Ast.eff_handler
  ; ret_id: Ast.ident
  ; ret_body: Ast.exp }
  [@@deriving compare]

and f_name = string

and env_t = value Ast.IdentMap.t [@@deriving compare]

(*and context_t = context_cell list [@@deriving compare] *)
(** Same as Context.t, it's defined here because of recursive datatypes *)
and context_t = context_cell list list [@@deriving compare]

type eval_result = Result of value

let value_of_eval_result = function Result x -> x

let value_equal = [%compare.equal : value]

let pp_context_cell fmt = function
  | Rator _ -> Format.fprintf fmt "rator"
  | Apply _ -> Format.fprintf fmt "apply"
  | Lift _ -> Format.fprintf fmt "lift"
  | Handle _ -> Format.fprintf fmt "handle"
  | TwoArgF1 _ -> Format.fprintf fmt "2arg-1"
  | TwoArgF2 _ -> Format.fprintf fmt "2arg-2"
  | Conditional _ -> Format.fprintf fmt "conditional"
  | LetCtx _ -> Format.fprintf fmt "letctx"


exception EvalException of string

module type CONTEXT = sig
  type cell = context_cell

  type t = context_t

  val empty : t

  val is_empty : t -> bool

  val push : v:cell -> t -> t

  val merge : t -> t -> t

  val head : t -> cell * t

  val split : t -> Ast.eff_name -> t * handle_context_data * t

  val pp : Format.formatter -> t -> unit
end

(** Naive context implementation where stack is represented as flat list.
    Because of mess with context_t type it's actually list of lists, but inner list has always one element *)
module NaiveContext : CONTEXT = struct
  type t = context_t

  type cell = context_cell

  let empty = []

  let is_empty context = match context with [] -> true | _ -> false

  let push ~v context = [v] :: context

  let merge top bottom = top @ bottom

  let head context = (List.hd_exn (List.hd_exn context), List.tl_exn context)

  let rec split_aux free_lvl searched_rev to_search search_eff =
    match to_search with
    | [] -> raise (EvalException "couldn't find handler")
    | ([(Handle ({eff} as data))] as head) :: rest
      when Ast.eff_name_equal eff search_eff ->
        if Int.equal free_lvl 0 then
          (List.rev (head :: searched_rev), data, rest)
        else split_aux (free_lvl - 1) (head :: searched_rev) rest search_eff
    | ([(Lift eff)] as head) :: rest when Ast.eff_name_equal eff search_eff ->
        split_aux (free_lvl + 1) (head :: searched_rev) rest search_eff
    | head :: rest -> split_aux free_lvl (head :: searched_rev) rest search_eff


  (** Splits context into two parts according to last rule from fig3 and
      returns them in following format:
      (handle_context :: E', handle_context, E) *)
  let split context eff = split_aux 0 [] context eff

  let pp fmt context =
    List.iter
      ~f:(fun cell ->
        Format.fprintf fmt "%a; " pp_context_cell (List.hd_exn cell) )
      context
end

(** Context implementation where stack is represented as list of lists.
    Invariant: each handle/lift operation is at the top of the group (inner list) *)
module StackedContext : CONTEXT = struct
  type t = context_t

  type cell = context_cell

  let empty = []

  let is_empty context = match context with [] -> true | _ -> false

  let push ~(v: cell) context =
    match context with
    | [] -> [[v]]
    | ((Handle _ | Lift _) :: _) :: _ -> [v] :: context
    | head_group :: rest -> (v :: head_group) :: rest


  (** Merge two contexts into one.
      WARNING: it may produce context with groups that don't start with handle/lift *)
  let merge top bottom = top @ bottom

  let head context =
    match context with
    | [] -> raise (EvalException "Context.head called on empty context")
    | [] :: rest ->
        raise
          (EvalException
             "Context.head: invariant violation: head group is empty")
    | [h] :: rest -> (h, rest) (* TODO this shouldn't need '[] ::' *)
    | (h :: group) :: rest -> (h, group :: rest)


  let rec split_aux free_lvl (searched_rev: t) (to_search: t) search_eff =
    let safe_cons x xs = match x with [] -> xs | _ -> x :: xs in
    match to_search with
    | [] -> raise (EvalException "couldn't find handler")
    | [] :: rest -> split_aux free_lvl searched_rev rest search_eff
    | ((Handle ({eff} as data) as head) :: rest_group as head_group) :: rest
      when Ast.eff_name_equal eff search_eff ->
        if Int.equal free_lvl 0 then
          (List.rev ([head] :: searched_rev), data, safe_cons rest_group rest)
        else
          split_aux (free_lvl - 1) (head_group :: searched_rev) rest search_eff
    | ((Lift eff) :: _ as head_group) :: rest
      when Ast.eff_name_equal eff search_eff ->
        split_aux (free_lvl + 1) (head_group :: searched_rev) rest search_eff
    | head_group :: rest ->
        split_aux free_lvl (head_group :: searched_rev) rest search_eff


  (** Splits context into two parts according to last rule from fig3 and
      returns them in following format:
      (handle_context :: E', handle_context, E) *)
  let split context eff = split_aux 0 [] context eff

  let pp fmt context =
    List.iter context ~f:(fun group ->
        List.iter group ~f:(fun cell ->
            Format.fprintf fmt "%a; " pp_context_cell cell ) )
end

(*module Context = NaiveContext *)
module Context = StackedContext

let pp_value fmt = function
  | IntVal v -> Format.fprintf fmt "int: %d" v
  | BoolVal v -> Format.fprintf fmt "bool: %b" v
  | Closure (arg, body, _) -> Format.fprintf fmt "%s -> %a" arg Ast.pp_exp body
  | RecClosure (pname, arg, body, _) ->
      Format.fprintf fmt "rec %s = %s -> %a" pname arg Ast.pp_exp body
  | ResumeClosure ctx -> Format.fprintf fmt "resume closure %a" Context.pp ctx
  | Operation (op, eff) -> Format.fprintf fmt "%s:%s" eff op
  | Unit -> Format.fprintf fmt "()"


module Env = struct
  type t = env_t

  let extend ~symbol ~value env = Ast.IdentMap.add symbol value env

  let empty : t = Ast.IdentMap.empty

  let apply ~symbol env = Ast.IdentMap.find symbol env
end

module BuiltinsImpl = struct
  let minus val1 val2 =
    match (val1, val2) with
    | IntVal v1, IntVal v2 -> IntVal (v1 - v2)
    | _ -> raise (EvalException "type error in minus")


  let times val1 val2 =
    match (val1, val2) with
    | IntVal v1, IntVal v2 -> IntVal (v1 * v2)
    | _ ->
        raise
          (EvalException
             (Format.asprintf "type error in times %a * %a" pp_value val1
                pp_value val2))


  let equal val1 val2 = BoolVal (value_equal val1 val2)

  let get_impl name =
    match name with
    | BuiltinsDecl.Minus -> minus
    | BuiltinsDecl.Times -> times
    | BuiltinsDecl.Equal -> equal
end

let to_eval_value env (value: Ast.value) : value =
  match value with
  | Ast.Var symbol -> (
    try Env.apply ~symbol env with Not_found ->
      raise (EvalException ("couldn't find " ^ symbol)) )
  | Ast.Lambda (arg, body) -> Closure (arg, body, env)
  | Ast.LambdaRec (pname, arg, body) -> RecClosure (pname, arg, body, env)
  | Ast.Operation (op, eff) -> Operation (op, eff)
  | Ast.IntVal v -> IntVal v
  | Ast.Unit -> Unit
  | Ast.True -> BoolVal true
  | Ast.False -> BoolVal false


let find_op op handle_data =
  let handler = handle_data.handler in
  let effect =
    List.find ~f:(fun effect -> Ast.eff_name_equal op effect.Ast.op) handler
  in
  effect


type prog_data = {effect_decls: Ast.effect_decl list}

let find_eff effect_decls eff =
  List.find
    ~f:(fun (eff_name, _) -> Ast.eff_name_equal eff_name eff)
    effect_decls


let check_eff_name {effect_decls} eff =
  match find_eff effect_decls eff with
  | Some _ -> ()
  | None -> raise (EvalException "unknown effect name")


let check_handler {effect_decls} eff handler =
  match find_eff effect_decls eff with
  | Some (_, op_names) ->
      let handler_ops = List.map ~f:(fun {Ast.op} -> op) handler in
      if List.equal ~equal:Ast.op_name_equal handler_ops op_names then ()
      else
        raise (EvalException "handler doesn't deal with all ops in the effect")
  | None -> raise (EvalException "unknown effect name")


let rec value_of_exp prog_data env e context =
  (* Format.fprintf Format.std_formatter "value_of %a\n" Ast.pp_exp e ; *)
  let res =
    match e with
    | Ast.Value v -> apply_context prog_data context (to_eval_value env v)
    | Ast.Apply (rator, rand) ->
        value_of_exp prog_data env rator
          (Context.push ~v:(Rator (env, rand)) context)
    | Ast.Lift (eff, exp) ->
        check_eff_name prog_data eff ;
        value_of_exp prog_data env exp (Context.push ~v:(Lift eff) context)
    | Ast.Handle (eff, body, handler, ret_id, ret_body) ->
        check_handler prog_data eff handler ;
        let new_context =
          Context.push context
            ~v:(Handle {env; eff; handler; ret_id; ret_body})
        in
        value_of_exp prog_data env body new_context
    | Ast.Minus (e1, e2) ->
        value_of_exp prog_data env e1
          (Context.push context ~v:(TwoArgF1 (BuiltinsDecl.Minus, env, e2)))
    | Ast.Times (e1, e2) ->
        value_of_exp prog_data env e1
          (Context.push context ~v:(TwoArgF1 (BuiltinsDecl.Times, env, e2)))
    | Ast.CmpEqual (e1, e2) ->
        value_of_exp prog_data env e1
          (Context.push context ~v:(TwoArgF1 (BuiltinsDecl.Equal, env, e2)))
    | Ast.IfElse (c, e1, e2) ->
        value_of_exp prog_data env c
          (Context.push context ~v:(Conditional (env, e1, e2)))
    | Ast.Let (v, e1, e2) ->
        value_of_exp prog_data env e1
          (Context.push context ~v:(LetCtx (env, v, e2)))
  in
  res


and apply_proc prog_data rator_val rand_val context =
  match rator_val with
  | Closure (arg, body, env) ->
      value_of_exp prog_data
        (Env.extend ~symbol:arg ~value:rand_val env)
        body context
  | RecClosure (pname, arg, body, env) ->
      value_of_exp prog_data
        ( Env.extend ~symbol:pname ~value:rator_val env
        |> Env.extend ~symbol:arg ~value:rand_val )
        body context
  | Operation (op, eff) ->
      let resume_context, handle_data, common_context =
        Context.split context eff
      in
      let effect_opt = find_op op handle_data in
      let effect = Option.value_exn effect_opt in
      (* TODO which env to use in resume?? *)
      let resume_value = ResumeClosure resume_context in
      let new_env =
        handle_data.env
        |> Env.extend ~symbol:effect.Ast.operation_arg ~value:rand_val
        |> Env.extend ~symbol:effect.Ast.resume_cont ~value:resume_value
      in
      (*Format.fprintf Format.std_formatter
        "common_context: %a\nhandled_context: %a\n" Context.pp common_context
        Context.pp handled_context ; *)
      value_of_exp prog_data new_env effect.Ast.body common_context
  | ResumeClosure resume_context ->
      (* note: context != common_context from the previous case *)
      apply_context prog_data (Context.merge resume_context context) rand_val
  | _ -> raise (EvalException "Expected lambda")


and apply_context prog_data context value : eval_result =
  (* Format.fprintf Format.std_formatter "context: %a\n" Context.pp context ; *)
  if Context.is_empty context then Result value
  else
    let hd_ctx, rest = Context.head context in
    match hd_ctx with
    | Rator (env, rand_exp) ->
        value_of_exp prog_data env rand_exp
          (Context.push ~v:(Apply value) rest (* TODO *))
    | Apply rator_val -> apply_proc prog_data rator_val value rest
    | Lift _ -> apply_context prog_data rest value
    | Handle {env; ret_id; ret_body} ->
        apply_context prog_data
          (Context.push rest ~v:(Apply (Closure (ret_id, ret_body, env))))
          value
    | TwoArgF1 (fn, env, e2) ->
        value_of_exp prog_data env e2
          (Context.push rest ~v:(TwoArgF2 (fn, value)))
    | TwoArgF2 (fn, v1) ->
        apply_context prog_data rest ((BuiltinsImpl.get_impl fn) v1 value)
    | Conditional (env, e1, e2) -> (
      match value with
      | BoolVal b ->
          if b then value_of_exp prog_data env e1 rest
          else value_of_exp prog_data env e2 rest
      | _ -> raise (EvalException "conditional should be of type bool") )
    | LetCtx (env, symbol, body) ->
        value_of_exp prog_data (Env.extend ~symbol ~value env) body rest


let value_of_prog (effect_decls, body) =
  value_of_exp {effect_decls} Env.empty body Context.empty
  |> value_of_eval_result
