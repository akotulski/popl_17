
Interpreter of untyped version of the calculus with algebraic effects from [following paper](./biernacki-al-popl18.pdf)

# Getting started
Prerequisites:
- [opam](https://opam.ocaml.org/doc/Install.html)
- ocaml version 4.05+

## Building

NOTE: Initial build may take a while because of opam installing dependent libraries (mainly `core` library).


One liner when getting started (run from project directory):
```
opam switch set kotulski_project --alias-of 4.05.0 && eval `opam config env` && ./build.sh
```
Command above assumes that opam will use separate switch to avoid polluting default installation


To build again after initial setup, simply run `./build.sh`

## Running
Interpreter:
```
cat $SOURCE_CODE_FILE | src/_build/default/run.exe
# for example:
cat examples/simple.txt | src/_build/default/run.exe
```

Unit tests:
```
src/_build/default/test.exe
```

# Language

Supported expressions:
- function application
- let
- letrec (recursive functions)
- if then else
- handle
- lift
- builtin functions for minus(`-`), multiply(`*`) and equals?(`==`)

Supported values:
- variables
- functions
- operations, they are namespaced with their effect name: `eff_name:op_name`
- int
- bool
- unit

Syntax is hybrid between C-like languages and lisp:
- handle/lift expressions are in C-like form: `handle(name, exp, handler, return x.exp)`
- function/operation calls are in lisp form: `(fn arg)`. Functions have exactly one argument, except for operation handlers and functions builtin into the language.
- function declarations are in lambda calculus form (function arguments before `.`, body afterwards): `lambda arg. body`

Text after `//` is considered a comment until the end of the line

Example code can be found in `examples` directory.

## Grammar
```
program = effects { <effect_list> } <exp>

value = lambda <var>. <exp> | <var> | <eff_name>:<op_name> | <int> | <bool> | <unit>

exp = <value> | 
      (<exp> <exp>) |
      let <var> = <exp> in <exp> |
      let rec <var> = <var> . <exp> in <exp> |
      if <exp> then <exp> else <exp> |
      handle (<eff_name>, <exp>, <handler>, return <arg>. <exp>) |
      lift(<eff_name>, <exp>) |
      (- <exp> <exp>) |
      (* <exp> <exp>) |
      (== <exp> <exp>)

handler = [";"-separated_list(<op_name> <arg> <resume_arg> . <exp>)]

effect_list = ","-separated_list (<effect_decl>)

effect_decl = eff_name [";"-separated_list(<op_name>)]

op_name, eff_name, var, arg, resume_arg = <identifier>
```

# Code structure
- `src/ast.ml` - code that defines how abstract syntax tree looks like
- `src/eval.ml` - code responsible for evaluating the program
- `src/run.ml` -  code of program that reads source code from stdin, parse it and evaluate it.
- `src/parser.mly` - parsing rules
- `src/lexer.mll` - lexing rules
- `src/test.ml` - unit tests


