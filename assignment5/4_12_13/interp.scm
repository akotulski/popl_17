(module interp (lib "eopl.ss" "eopl")
  
  ;; interpreter for the EXPLICIT-REFS language

  (require "drscheme-init.scm")

  (require "lang.scm")
  (require "data-structures.scm")
  (require "environments.scm")
  (require "store.scm")
  (require racket/list) ; for last
  
  (provide value-of-program value-of instrument-let instrument-newref)

;;;;;;;;;;;;;;;; switches for instrument-let ;;;;;;;;;;;;;;;;

  (define instrument-let (make-parameter #f))

  ;; say (instrument-let #t) to turn instrumentation on.
  ;;     (instrument-let #f) to turn it off again.

;;;;;;;;;;;;;;;; the interpreter ;;;;;;;;;;;;;;;;

  ;; value-of-program : Program -> ExpVal
  ;; Page: 110
  (define value-of-program 
    (lambda (pgm)
      (cases program pgm
        (a-program (exp1)
          (value-of exp1 (init-env) (empty-store))))))

  (define (get-store-from-ans ans)
    (cases answer ans
           (an-answer (val new-store) new-store)))

  ;; value-of : Exp * Env -> ExpVal
  ;; Page: 113
  (define value-of
    (lambda (exp env store)
      (cases expression exp

        ;\commentbox{ (value-of (const-exp \n{}) \r) = \n{}}
        (const-exp (num) (an-answer (num-val num) store))

        ;\commentbox{ (value-of (var-exp \x{}) \r) = (apply-env \r \x{})}
        (var-exp (var) (an-answer (apply-env env var) store)) ; TODO apply-store?? Why?

        ;\commentbox{\diffspec}
        (diff-exp
         (exp1 exp2)
         (cases answer (value-of exp1 env store)
                (an-answer
                 (val1 store1)
                 (cases answer (value-of exp2 env store1)
                        (an-answer
                         (val2 store2)
                         (let ((num1 (expval->num val1))
                               (num2 (expval->num val2)))
                           (an-answer
                            (num-val (- num1 num2))
                             store2)))))))
      
        ;\commentbox{\zerotestspec}
        (zero?-exp
         (exp1)
         (cases answer (value-of exp1 env store)
                (an-answer
                 (val1 store1)
                 (let ((num1 (expval->num val1)))
                   (if (zero? num1)
                       (an-answer (bool-val #t) store1) 
                       (an-answer (bool-val #f) store1))))))
              
        ;\commentbox{\ma{\theifspec}}
        (if-exp
         (exp1 exp2 exp3)
         (cases answer (value-of exp1 env store)
                (an-answer
                 (val1 store1)
                 (if (expval->bool val1)
                     (value-of exp2 env store1)
                     (value-of exp3 env store1)))))

        ;\commentbox{\ma{\theletspecsplit}}
        (let-exp
         (var exp1 body)
         (cases answer (value-of exp1 env store)
                (an-answer
                 (val1 store1)
                 (value-of body
                           (extend-env var val1 env) store1))))

        (proc-exp (vars body)
          (an-answer (proc-val (procedure vars body env)) store))

        (call-exp
         (rator rands)
         (cases answer (value-of rator env store)
                (an-answer
                 (val-rator store-rator)
                 (letrec
                     ((value-of-rands ; this function returns list of evaluated arguments + final store at last element
                       (lambda (rands env store)
                         (if (null? rands)
                             (list store)
                             (cases answer (value-of (car rands) env store)
                                    (an-answer
                                     (val new-store)
                                     (cons val (value-of-rands (cdr rands) env new-store))))))))
                   (apply-procedure
                    (expval->proc val-rator)
                    (value-of-rands rands env store-rator))))))

        (letrec-exp (p-names b-vars p-bodies letrec-body)
          (value-of letrec-body
            (extend-env-rec* p-names b-vars p-bodies env) store))

        (begin-exp (exp1 exps)
          (letrec
            ((value-of-begins
               (lambda (e1 es store)
                 (let* ((a1 (value-of e1 env store))
                        (store1 (get-store-from-ans a1)))
                   (if (null? es)
                     a1
                     (value-of-begins (car es) (cdr es) store1))))))
            (value-of-begins exp1 exps store)))

        (newref-exp
         (exp1)
         (cases answer (value-of exp1 env store)
                (an-answer
                 (v1 store1)
                 (let ((ref-res (newref store1 v1)))
                   (an-answer (ref-val (car ref-res)) (cadr ref-res))))))

        (deref-exp
         (exp1)
         (cases answer (value-of exp1 env store)
                (an-answer (v1 store1)
                           (let ((ref1 (expval->ref v1)))
                             (an-answer (deref store1 ref1) store1)))))

        (setref-exp
         (exp1 exp2)
         (cases answer (value-of exp1 env store)
                (an-answer
                 (val1 store1)
                 (cases answer (value-of exp2 env store1)
                        (an-answer
                         (v2 store2)
                         (let ((store3 (setref! store2 (expval->ref val1) v2)))
                           (an-answer (num-val 23) store3)))))))
        )))

  ;; apply-procedure : Proc * (cons (list-of ExpVal) Store) -> ExpVal
  ;; 
  ;; uninstrumented version
  ;;   (define apply-procedure
  ;;    (lambda (proc1 arg)
  ;;      (cases proc proc1
  ;;        (procedure (bvar body saved-env)
  ;;          (value-of body (extend-env bvar arg saved-env))))))

  ;; instrumented version
  (define apply-procedure
    (lambda (proc1 args+store)
      (cases proc proc1
             (procedure
              (vars body saved-env)
              (letrec
                  ((ext-env*
                    (lambda (vars args+store env)
                      (if (null? vars)
                          env
                          (ext-env*
                           (cdr vars)
                           (cdr args+store)
                           (extend-env (car vars) (car args+store) env))))))
                (let ((new-env (ext-env* vars args+store saved-env))
                      (store (last args+store)))
                  (when (instrument-let)
                        (begin
                          (eopl:printf
                           "entering body of proc ~s with env =~%"
                           (car vars))
                          (pretty-print (env->list new-env))
                          (eopl:printf "store =~%")
                          (pretty-print (store->readable (get-store-as-list store)))
                          (eopl:printf "~%")))
                  (value-of body new-env store)))))))


  ;; store->readable : Listof(List(Ref,Expval)) 
  ;;                    -> Listof(List(Ref,Something-Readable))
  (define store->readable
    (lambda (l)
      (map
        (lambda (p)
          (cons
            (car p)
            (expval->printable (cadr p))))
        l)))
  
  )
  


  
