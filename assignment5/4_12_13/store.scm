(module store (lib "eopl.ss" "eopl")
  
  (require "drscheme-init.scm")
  (require racket/list) ; for take
  (require racket/vector) ; for vector-append
   
  (provide empty-store reference? newref deref setref!
    instrument-newref get-store-as-list)
  
  (define instrument-newref (make-parameter #f))
  
  ;;;;;;;;;;;;;;;; references and the store ;;;;;;;;;;;;;;;;
  
  ;;; world's dumbest model of the store:  the store is a list and a
  ;;; reference is number which denotes a position in the list.

  ;; empty-store : () -> Sto
  ;; Page: 111
  (define empty-store
    (lambda () (list 0 (make-vector 1))))
  
  ;; reference? : SchemeVal -> Bool
  ;; Page: 111
  (define reference?
    (lambda (v)
      (integer? v)))

  ;; newref : ExpVal -> Ref
  ;; Page: 111
  (define newref
    (lambda (store val)
      (let* ((next-ref (car store))
             (next-vec ; make vector 2x larger if next-ref too large
             (if (< next-ref (vector-length (cadr store)))
                 (cadr store)
                 (vector-append (cadr store) (make-vector next-ref))))
             (new-store (list (+ 1 next-ref) next-vec)))
        (vector-set! next-vec next-ref val)
        (when (instrument-newref)
            (eopl:printf 
             "newref: allocating location ~s with initial contents ~s~%"
             next-ref val))
        (list next-ref new-store))))

  ;; deref : Ref -> ExpVal
  ;; Page 111
  (define deref 
    (lambda (store ref)
      (vector-ref (cadr store) ref)))

  ;; setref! : Ref * ExpVal -> Unspecified
  ;; Page: 112
  (define setref!                       
    (lambda (store ref val)
      (vector-set! (cadr store) ref val)
      store))
  (define report-invalid-reference
    (lambda (ref the-store)
      (eopl:error 'setref
        "illegal reference ~s in store ~s"
        ref the-store)))

  ;; get-store-as-list : () -> Listof(List(Ref,Expval))
  ;; Exports the current state of the store as a scheme list.
  ;; (get-store-as-list '(foo bar baz)) = ((0 foo)(1 bar) (2 baz))
  ;;   where foo, bar, and baz are expvals.
  ;; If the store were represented in a different way, this would be
  ;; replaced by something cleverer.
  ;; Replaces get-store (p. 111)
   (define get-store-as-list
     (lambda (store)
       (letrec
         ((inner-loop
            ;; convert sto to list as if its car was location n
            (lambda (sto n)
              (if (null? sto)
                '()
                (cons
                  (list n (car sto))
                  (inner-loop (cdr sto) (+ n 1)))))))
         (inner-loop
          (take
           (vector->list
            (cadr store))
           (car store))
          0))))

  )
